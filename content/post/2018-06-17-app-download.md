---
title: Uvolnění aplikace XMLVideoSharp ke stažení
date: 2018-06-17
tags: ["uvolění", "aplikace", "XMLVideoSharp", "download", "ke", "stažení", "verze", "1.0"]
---

Aplikace XMLVideoSharp je nyní volně dostupná ke stažení jako zip archiv na těchto
webových stránkách v sekci [Download](https://aokinanuk.gitlab.io/hugo/page/download/).

Nebo můžete ke stažení použít rovnou tento [odkaz](https://gitlab.com/AokiNanuk/hugo/raw/master/App/XMLVideoSharpApp.zip).

Děkujeme že využíváte naši aplikaci!

Tým VideoSharp