---
title: XML databáze != relační databáze
date: 2018-06-01
tags: ["návrh", "aplikace", "0.2", "alpha", "nativní", "XML", "není", "relační", "databáze"]
---

Jednou z neodmyslitelných součástí vývoje jakékoliv aplikace jsou omyly. Při
podrobnějším zkoumání a počátcích implementace samotné aplikace jsme narazili
na jeden vážný problém ...

<!--more-->

Aplikace ukládající data do nativní XML databáze nelze navrhovat jako aplikace
pracující s relační databází. Neboť oba tyto systémy mají trošku jiné hodnoty a
cíle, k nimž je optimální je využívat.

## Co je to nativní XML databáze?
Co tedy vlastně rozumíme pod pojmem *nativní XML databáze*? Odpovědět na tuto
otázku nebylo úplně snadné. Z dostupných zdrojů jsme využili vágnější z definic,
konkrétně: 

> *"Nativní XML databáze je databáze, jejíž základní jednotkou je soubor XML."*

Základní jednotkou relačních datbází je tuple (n-tice), tedy 1 řádek tabulky. 
Platí tedy, že v nativní XML databázi 1 soubor reprezentuje 1 řádek pomyslné
"tabulky" v naší databázi.

## Jaká je výhoda nativní XML databáze?
Proč je tedy dobré nativní XML databáze využívat? Výhodou je zejména jejich 
flexibilita, která z nich činí lákavou možnost pro ukládání dat. Nativní XML 
databáze mohou být vybudovány prakticky kdekoliv - ať už se jedná o 
specializované úložiště, či běžný file system. Rovněž nejsou tak striktně
svazovány pravidly, jako relační databáze. V nativní XML databázi si můžeme 
v některých "řádcích" dovolit vynechat některé "sloupce" bez SQL errorů.

Jak už to však na světě bývá, výhody nám často vyvažují nevýhody. Je tedy 
potřeba říci, že operace nad nativními XML databázemi zpravidla nebývají
rychlejší, než relační, právě kvůli své větší flexibilitě.

## Závěrem
Nyní už tedy víme, co to zhruba je nativní XML databáze a nic nám tedy nebrání 
znovu se vrhnout na navrhování a implementace lepší, čistší a kvalitnější
aplikace.

VideoSharp development tým