---
title: Závěrečné zprávy o projektu
comments: false
---

### Radek Václavek, vedoucí projektu
[PDF]
(https://gitlab.com/AokiNanuk/hugo/raw/master/content/reports/pdf/report_Vaclavek.pdf)/[Docbook]
(https://gitlab.com/AokiNanuk/hugo/raw/master/content/reports/docbook/report_Vaclavek.xml)

### Jan Breburda, hlavní vývojář
[PDF]
(https://gitlab.com/AokiNanuk/hugo/raw/master/content/reports/pdf/report_Breburda.pdf)/[Docbook]
(https://gitlab.com/AokiNanuk/hugo/raw/master/content/reports/docbook/report_Breburda.xml)

### Tomáš Sedláček, vývojář, návrhář
[PDF]
(https://gitlab.com/AokiNanuk/hugo/raw/master/content/reports/pdf/report_Sedlacek.pdf)/[Docbook]
(https://gitlab.com/AokiNanuk/hugo/raw/master/content/reports/docbook/report_Sedlacek.xml)