---
title: Ukládání dat
date: 2018-06-10
tags: ["návrh", "aplikace", "0.2", "alpha", "ukládání", "dat"]
---

V jakékoliv aplikaci spravující databázi je třeba si ujasnit, jak budou
ukládána samotná data. V tomto příspěvku se proto podíváme právě na formát
XML dokumentů určených v naší aplikaci právě k tomuto účelu.

<!--more-->


Data ukládáme do dvou různých typů XML dokumentů:

### Seznam kategorií
Obsahuje seznam všech kategorií, do nichž lze ukládat média. Popisuje také atributy
jednotlivých kategorií. 
K rozhodnutí ukládat informace o kategorii zvlášť nás vedlo několik faktorů, 
primárně šlo však o fakt, že ze zadání a z ukázkového souboru je zřejmé, že atributy
jsou vázány právě na kategorii, nikoliv na média samotná.

[XMLSchema pro seznam kategorií](https://gitlab.com/AokiNanuk/hugo/raw/master/content/xml/arrayofcategory.xsd)

### Médium
Obsahuje ID média (rozhodli jsme se pro unikátní ID v rámci všech médií, ID v 
rámci kategorie by postrádalo logický smysl) a atributy odpovídající kategorii, 
v níž je médium zařazeno.

Médium se přiřazuje do kategorie již při vytváření a tedy atributy kopíruje 
přímo z dané kategorie.

Médium je možné přesunout mezi kategoriemi **pouze** pokud jsou vyplněné atributy
média podmnožinou atributů cílové kategorie. Tento přístup byl zvolen kvůli 
zachování smysluplnosti obsahu atributů při přesunu média mezi kategoriemi.

Médium musí být vždy přiřazeno k nějaké kategorii.

[XMLSchema pro médium](https://gitlab.com/AokiNanuk/hugo/raw/master/content/xml/medium.xsd)