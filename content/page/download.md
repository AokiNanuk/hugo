---
title: Download
subtitle: Stáhněte si aplikaci XMLVideo
comments: false
---

## Verze 1.0 (ZIP)
[Download XMLVideo 1.0](https://gitlab.com/AokiNanuk/hugo/raw/master/App/XMLVideoSharpApp.zip)

Děkujeme že využíváte naši aplikaci!

**Tým VideoSharp**