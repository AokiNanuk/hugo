## Krátké info

Tato stránka je dokumentačním blogem o vývoji aplikace XMLVideoSharp pro správu
domácí videotéky uložené v nativní XML databázi.

Tato stránka byla vytvořena za pomoci [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io). Využívá styl `beautifulhugo`.


