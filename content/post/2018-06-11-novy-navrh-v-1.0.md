---
title: Nový návrh aplikace (v. 1.0)
date: 2018-06-11
tags: ["nový", "návrh", "aplikace", "verze", "1.0"]
---

Při implementaci aplikace lze nezřídka zjistit, že původní návrh není úplně
optimální. Podobně se to stalo i nám a proto jsme se rozhodli začít od píky a
vytvořili jsme úplně nový návrh, který byl nakonec v této podobě také
implementován.

<!--more-->


Obecně vzato jsme se vydali cestou zjednodušování a zpřehledňování - jak
samotného návrhu, tak i zpracování aplikace.

### Use case diagram
Rozhodli jsme se znovu identifikovat a definovat případy užití naší aplikace,
neboť původní návrh byl zbytečně složitý.

![UseCaseDiagram](https://gitlab.com/AokiNanuk/hugo/raw/master/content/pictures/newdesign/pb138usecase.jpg "Use case diagram")

### Class diagram
I class diagram doznal určitých změn, co se architektury i přehlednosti týká.
Návrh je nyní přehlednější, jasnější a jednodušší.

![ClassDiagram](https://gitlab.com/AokiNanuk/hugo/raw/master/content/pictures/newdesign/pb138classDiagram.jpg "Class diagram")

### GUI
V grafickém uživatelském rozhraní došlo k asi nejrazantnějším úpravám. Nejenže
jsme se rozhodli sloučit funkcionalitu aplikace do jednoho kompaktního okna, 
zároveň zobrazujeme databázi jako přehlednou, stromovou strukturu.

![GUI](https://gitlab.com/AokiNanuk/hugo/raw/master/content/pictures/newdesign/visualsharpapp.png "GUI")

*Obrázek GUI pochází přímo z funkční aplikace*

### Závěrem
Věříme, že zpřehlednění a zjednodušení návrhu aplikace jen zvýšilo kvalitu
a uživatelskou přívětivost aplikace VideoSharp. Samotná aplikace bude již brzy
dostupná ke stažení a tedy si budete sami moct vyzkoušet, zda tomu tak skutečně
je!

Tým VisualSharp